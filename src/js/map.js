class GMap {

  constructor() {
    this.initMap();
    this.icon = {
      url: "./images/bicycle.png",
      scaledSize: new google.maps.Size(20, 20),
    }

  }


  initMap() {

    this.map = new google.maps.Map(document.getElementById('map'), {
      center: {
        lat: 52.229675,
        lng: 21.012230
      },
      zoom: 8
    });
  }

  addMarker(lat, lng) {
    this.marker = new google.maps.Marker({
      position: {
        lat: lat,
        lng: lng
      },
      map: this.map,
      icon: this.icon
    })
    this.marker.setMap(this.map)

  }

  showMarkers(markers) {

    markers.forEach(marker => {
      this.addMarker(marker.lat,marker.lng)
      this.marker.setMap(this.map)
    })
  }

  deleteMarker() {

    this.marker.setMap(null)
    this.marker = null;
  }

  updateMarker(lat,lng) {
    const position = new google.maps.LatLng(lat,lng);
    this.marker.setPosition(position);
    this.deleteMarker();
    this.addMarker(lat,lng)

  }
}


