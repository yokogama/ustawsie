// http class

class HTTPRequests {

  //just empty constructor for now
  constructor() {

  }

  //GET request
  async get(url) {
    const response = await fetch(url, {
      method: 'GET'
    });
    const resData = await response.json();
    return resData;
  }

  //POST request
  async post(url, data) {
    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify(data)
    });
    const resData = await response.json();
    return resData;
  }

  //DELETE request
  async delete(url) {
    const response = await fetch(url, {
      method: 'DELETE',
      headers: {
        'Content-type': 'application/json'
      }
    });
    return response;
  }

  async put(url, data) {
    const response = await fetch(url, {
      method: 'PUT',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(data)
    });
    const resData = await response.json();
    return resData;
  }
}