//get the html elements
const btnSubmit = document.querySelector('#btnSubmit');
const meetingsList = document.querySelector('.meetingsList');
const pagination = document.querySelector('.pagination');

const meetingsListItem = Array.from(document.querySelectorAll('.meetingsList__item'));
const formName = document.querySelector('#formName');
const formTitle = document.querySelector('#formTitle');
const formLocalization = document.querySelector('#formLocalization');
const formDate = document.querySelector('#formDate');
const formTime = document.querySelector('#formTime');
const formBike = document.querySelector('#formBike');
const formPeople = document.querySelector('#formPeople');
const formDuration = document.querySelector('#formDuration');
const formDistance = document.querySelector('#formDistance');
const formLevel = document.querySelector('#formLevel');
const formDescription = document.querySelector('#formDescription');
const formInfo = document.querySelector('#formInfo');
const box1 = document.querySelector('.section__box1');
const sectionTitle = document.querySelector('.section__title');
const meetingCardWrapper = document.querySelector('.meetingCardWrapper');
const box4 = document.querySelector('.section__box4');
const cancelBtn = document.querySelector('#cancel-btn');
const meetingCard = document.querySelector('.meetingCard');
const searchInput = document.querySelector('#searchInput');
let id = 0;
let currentPage = 1;
let resultsPerPage = 5;
let page;

//set actual Date and Time
const now = new Date();
const limitDate = '2050.01.01';

const year = now.getFullYear();
const month = now.getMonth() < 10 ? `0${now.getMonth()+1}` : now.getMonth();
const day = now.getDate() < 10 ? `0${now.getDate()}` : now.getDate();
const actualDate = `${year}-${month}-${day}`;
const minutes = now.getMinutes() < 10 ? `0${now.getMinutes()}` : now.getMinutes();
const hours = now.getHours() < 10 ? `0${now.getHours()}` : now.getHours();
const actualTime = `${hours}:${minutes}`;

// App

// make a object of http

url = 'http://localhost:1337/meetings';

const http = new HTTPRequests();

// make an object of UI
const ui = new UI();
const map = new GMap();


// function getMeetings
const getMeetings = () => {
  //make a get request
  http.get(`${url}?date_gte=${actualDate}`)

    .then(data => {
      ui.showMeetings(data)
      ui.showPagination(data)
      map.showMarkers(data)

    })
    .catch(error => console.log(error));

}

//setPagination
const setPagination = (event) => {
  //set actual page number as button number
  currentPage = event.target.innerText;

  getMeetings();
  event.preventDefault();
}


//function sendMeeting
const sendMeeting = (event) => {


  // get the value of Outputs
  const name = formName.value;
  const title = formTitle.value;
  const localization = formLocalization.value;
  const date = formDate.value;
  const time = formTime.value;
  const bike = formBike.value
  const people = formPeople.value;
  const duration = formDuration.value;
  const distance = formDistance.value;
  const level = formLevel.value;
  const description = formDescription.value;
  const info = formInfo.value;
  let lat;
  let lng;

  console.log(date, time)
  console.log(actualDate, actualTime)
  if (name === '' || title === '' || localization === '' || date === '' || time === '' || bike === '' || people === '' || duration === '' || distance === '' || level === '' || description === '' || info === '') {
    ui.showAlert('Musisz wypełnic wszystkie pola poprawnie', 'alert alert--wrong');
  } else if (`${date} ${time}` < `${actualDate} ${actualTime}` || date >= limitDate) {
    ui.showAlert('Nie mozesz dodać ustawki wcześniej niz aktualna data', 'alert alert--wrong');
  } else {

    let key = 'AIzaSyDi81FTiLpb9-JBYH7Exkq-MSRFs3T7yG4';
    http.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${localization}&key=${key}`)
      .then(data => {
        lat = data.results[0].geometry.location.lat;
        lng = data.results[0].geometry.location.lng;


        //make summary data
        const sumData = {
          id,
          name,
          title,
          localization,
          date,
          time,
          bike,
          people,
          duration,
          distance,
          level,
          description,
          info,
          lat,
          lng
        }

        // if there is the same id then update meeting

        if (id === btnSubmit.dataset.id) {


          http.put(`${url}/${id}`, sumData)
            .then(() => {

              map.updateMarker(sumData.lat, sumData.lng);
              getMeetings(sumData);

              ui.showCard(sumData);
              ui.showAlert('Ustawka zaktualizowana', 'alert alert--success');
              ui.clearForm();
              btnSubmit.dataset.id = '';
              btnSubmit.textContent = 'Zorganizuj ustawkę';
              cancelBtn.style.display = 'none';

            })
            .catch(error => console.log(error));

        } else {
          //create new meeting
          http.post(url, sumData)
            .then(() => {
              map.addMarker(sumData.lat, sumData.lng);
              getMeetings(sumData);
              ui.showAlert('Ustawka dodana', 'alert alert--success');
              ui.clearForm();
              ui.hideCard();

            })
            .catch(error => console.log(error));
        }


      })
      .catch(error => console.log(error));
  }



  event.preventDefault();

}

//function to show meeting card
const toggleCard = (event) => {

  //getting data from dataset
  if (event.target.parentElement.classList.contains('meetingsList__item')) {
    id = event.target.parentElement.dataset.id;

    const title = event.target.parentElement.dataset.title;
    const date = event.target.parentElement.dataset.date;
    const time = event.target.parentElement.dataset.time;
    const name = event.target.parentElement.dataset.name;
    const localization = event.target.parentElement.dataset.localization;
    const bike = event.target.parentElement.dataset.bike;
    const people = event.target.parentElement.dataset.people;
    const duration = event.target.parentElement.dataset.duration;
    const distance = event.target.parentElement.dataset.distance;
    const level = event.target.parentElement.dataset.level;
    const description = event.target.parentElement.dataset.description;
    const info = event.target.parentElement.dataset.info;

    const params = {
      id,
      title,
      date,
      time,
      name,
      localization,
      bike,
      people,
      duration,
      distance,
      level,
      description,
      info

    }

    //if there is a clicked meeting from the list then showCard of meeting
    if (id) {
      ui.showCard(params);

    }
  }
  event.preventDefault();
}

//function to delete meeting
const deleteMeeting = (event) => {

  if (event.target.id === 'delete-btn') {
    id = event.target.dataset.id;

    if (confirm('Chcesz usunąc ustawke?')) {
      http.delete(`${url}/${id}`)
        .then(sumData => {
          map.deleteMarker();
          getMeetings(sumData);
          ui.hideCard();
          ui.showAlert('Ustawka skasowana', 'alert alert--success')
        })
        .catch(error => console.log(error));
    }
  }


  event.preventDefault();
}

//function for update meeting
const updateMeeting = (event) => {

  if (event.target.id === 'update-btn') {

    //getting data from meetingCard
    id = event.target.dataset.id;
    const name = event.target.parentElement.previousElementSibling.children[0].children[1].textContent;
    const title = event.target.parentElement.previousElementSibling.children[1].children[1].textContent;
    const localization = event.target.parentElement.previousElementSibling.children[2].children[1].textContent;
    const date = event.target.parentElement.previousElementSibling.children[3].children[1].textContent;
    const time = event.target.parentElement.previousElementSibling.children[4].children[1].textContent;
    const bike = event.target.parentElement.previousElementSibling.children[5].children[1].textContent;
    const people = event.target.parentElement.previousElementSibling.children[6].children[1].textContent;
    const duration = event.target.parentElement.previousElementSibling.children[7].children[1].textContent;
    const distance = event.target.parentElement.previousElementSibling.children[8].children[1].textContent;
    const level = event.target.parentElement.previousElementSibling.children[9].children[1].textContent;
    const description = event.target.parentElement.previousElementSibling.children[10].children[1].textContent;
    const info = event.target.parentElement.previousElementSibling.children[11].children[1].textContent;


    const data = {
      id,
      name,
      title,
      localization,
      date,
      time,
      bike,
      people,
      duration,
      distance,
      level,
      description,
      info
    }

    // fill the form with data above

    ui.updateForm(data);



    event.preventDefault();
  }
}

//function for quit from editing meeting
const cancelEditing = (event) => {
  event.preventDefault();
  if (event.target.id === 'cancel-btn') {
    ui.clearForm();
    btnSubmit.textContent = 'Zorganizuj ustawkę';
    btnSubmit.dataset.id = '';
    cancelBtn.style.display = 'none';

  }

}

const searchMeetings = (event) => {
  const searchPhrase = searchInput.value;

  if (searchPhrase === '') {

    ui.showAlert('Pole wyszukiwania nie moze być puste!', 'alert alert--wrong');

  }

  http.get(`${url}?date_gte=${actualDate}`)
    .then(data => {
      ui.showMeetings(data.filter(data => data.title.toLowerCase().includes(searchPhrase.toLowerCase())));

    })
    .catch(error => console.log(error));



  event.preventDefault();
}

// Listener for load all meetings
document.addEventListener('DOMContentLoaded', getMeetings);

//listener for add meeting
btnSubmit.addEventListener('click', sendMeeting);

// listener for toggle a meeting card
meetingsList.addEventListener('click', toggleCard);

//listener for delete meeting
meetingCardWrapper.addEventListener('click', deleteMeeting);

//listener for edit meeting
meetingCardWrapper.addEventListener('click', updateMeeting);

//listener for cancel btn
cancelBtn.addEventListener('click', cancelEditing);

//listener for searching meetings

searchInput.addEventListener('keyup', searchMeetings);

//listener for pagination buttons
pagination.addEventListener('click', setPagination);