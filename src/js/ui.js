//UI class

class UI {
  constructor() {

    this.formName = document.querySelector('#formName');
    this.formTitle = document.querySelector('#formTitle');
    this.formLocalization = document.querySelector('#formLocalization');
    this.formDate = document.querySelector('#formDate');
    this.formTime = document.querySelector('#formTime');
    this.formBike = document.querySelector('#formBike');
    this.formPeople = document.querySelector('#formPeople');
    this.formDuration = document.querySelector('#formDuration');
    this.formDistance = document.querySelector('#formDistance');
    this.formLevel = document.querySelector('#formLevel');
    this.formDescription = document.querySelector('#formDescription');
    this.formInfo = document.querySelector('#formInfo');



  }


  //Rendering meeting List
  showMeetings(meetings) {

    //sorting first by date, second by time, third by name
    meetings.sort((a,b) => {


      if(a.date === b.date) {

        if(a.time === b.time) {
          return a.title > b.title ? 1 : -1;
        }
        return a.time > b.time ? 1 : -1;
      }

      return a.date > b.date ? 1 : -1;


    });

    // meetings.sort((a, b) => (a.date > b.date) ? 1 : (a.date === b.date) ? ((a.title > b.title) ? 1 : -1) : -1 )

    //make a empty var for a render one meeting item
    let output = '';

    //set how many meetings per one page will be display



    const startPoint = (currentPage-1)*resultsPerPage;
    const endPoint = startPoint + resultsPerPage;
    const slicedMeetings = meetings.slice(startPoint, endPoint);


    //a loop through our data to render a html meeting item
    slicedMeetings.forEach(meeting => {

      output += `
        <li class="meetingsList__item" data-id="${meeting.id}" data-bike="${meeting.bike}" data-localization="${meeting.localization}" data-people="${meeting.people}" data-duration="${meeting.duration}" data-distance="${meeting.distance}" data-level="${meeting.level}" data-description="${meeting.description}" data-info="${meeting.info}" data-title="${meeting.title}" data-date="${meeting.date}" data-name="${meeting.name}" data-time="${meeting.time}">
            <div class="meetingsList__text" id="title">${meeting.title}</div>
            <div class="meetingsList__text" id="date">${meeting.date}</div>
            <div class="meetingsList__text" id="time">${meeting.time}</div>
            <div class="meetingsList__text" id="name">${meeting.name}</div>
        </li>
      `;


    });

      //add the output to html
      meetingsList.innerHTML = output;

  }
  //show pagination
  showPagination(meetings) {
    //make a empty var for a render paginaton buttons
    let paginationOutput = '';

    //how many buttons to display
    let pages = Math.ceil(meetings.length / resultsPerPage);
    // looping through to display buttons
    for(let i=1; i<=pages; i++) {
      page = i;
      //if we have the same amount of meetings as results we set
      if(meetings.length <= resultsPerPage) {
        //then we won't display buttons
        paginationOutput = '';
      } else {
        //display buttons
      let className = 'pagination__link';
      if(currentPage == page) {
         className += ' pagination__link--active';
      }

      paginationOutput += `<li class="pagination__item"><button class="${className}">${page}</button></li>`;

      }


    }
      //add the output to html
      pagination.innerHTML = paginationOutput;

  }


  //showCard
  showCard(params) {
    let output = '';

      output = ` <ul class="meetingCard" data-id="${params.id}">
      <li class="meetingCard__item"><span class="meetingCard__title">Organizator: </span><span class="meetingCard__text">${params.name}</span></li>
      <li class="meetingCard__item"><span class="meetingCard__title">Nazwa: </span><span class="meetingCard__text">${params.title}</span></li>
      <li class="meetingCard__item"><span class="meetingCard__title">Lokalizacja: </span><span class="meetingCard__text">${params.localization}</span></li>
      <li class="meetingCard__item"><span class="meetingCard__title">Data: </span><span class="meetingCard__text">${params.date}</span></li>
      <li class="meetingCard__item"><span class="meetingCard__title">Godzina: </span><span class="meetingCard__text">${params.time}</span></li>
      <li class="meetingCard__item"><span class="meetingCard__title">Rower: </span><span class="meetingCard__text">${params.bike}</span></li>
      <li class="meetingCard__item"><span class="meetingCard__title">Liczba osób: </span><span class="meetingCard__text">${params.people}</span></li>
      <li class="meetingCard__item"><span class="meetingCard__title">Czas trwania [h]: </span><span class="meetingCard__text">${params.duration}</span></li>
      <li class="meetingCard__item"><span class="meetingCard__title">Trasa [km]: </span><span class="meetingCard__text">${params.distance}</span></li>
      <li class="meetingCard__item"><span class="meetingCard__title">Poziom trudności [1-3]: </span><span class="meetingCard__text">${params.level}</span></li>
      <li class="meetingCard__item"><span class="meetingCard__title">Opis trasy: </span><span class="meetingCard__text">${params.description}</span></li>
      <li class="meetingCard__item"><span class="meetingCard__title">Dodatkowe informacje: </span><span class="meetingCard__text">${params.info}</span></li>


    </ul>
    <div class="btnGroup btnGroup--active"> <!-- btn--active-->
      <button class="btn btn--yellow" id="update-btn" data-id="${params.id}">Zaktualizuj ustawkę</button>
      <button class="btn btn--red" id="delete-btn" data-id="${params.id}">Skasuj ustawkę</button>

    </div>`;

    meetingCardWrapper.innerHTML = output;
  }

  hideCard() {
    let output = '';
    meetingCardWrapper.innerHTML = output;
  }

  //fill form inputs to update
  updateForm(data) {
    this.formName.value = data.name;
    this.formTitle.value = data.title;
    this.formLocalization.value = data.localization;
    this.formDate.value = data.date;
    this.formTime.value = data.time;
    this.formBike.value = data.bike;
    this.formPeople.value = data.people;
    this.formDuration.value = data.duration;
    this.formDistance.value = data.distance;
    this.formLevel.value = data.level;
    this.formDescription.value = data.description;
    this.formInfo.value = data.info;


    btnSubmit.textContent = 'Zaktualizuj ustawkę';
    btnSubmit.dataset.id = data.id;
    cancelBtn.style.display = 'block';
  }
  //show alert if the meeting is added
  showAlert(msg, className) {

    //clear alerts if there are any
    this.clearAlert();
    //create div
    const div = document.createElement('div');
    //add class
    div.className = className;
    //add text
    div.appendChild(document.createTextNode(msg));
    //insert to html
    box1.insertBefore(div,sectionTitle);

    // alert disapear after 2s
    setTimeout(() => {
      this.clearAlert();

    },2000);
  }

  // delete alert
  clearAlert() {
    const alert = document.querySelector('.alert');

    if(alert) {
      alert.remove();
    }
  }
  // clear form Outputs after submit
  clearForm() {
    this.formName.value = '';
    this.formTitle.value = '';
    this.formLocalization.value = '';
    this.formDate.value = '';
    this.formTime.value = '';
    this.formBike.value = '';
    this.formPeople.value = '';
    this.formDuration.value = '';
    this.formDistance.value = '';
    this.formLevel.value = '';
    this.formDescription.value = '';
    this.formInfo.value = '';

  }
}
